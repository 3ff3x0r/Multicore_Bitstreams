# Sega Master System e Game Gear

Reset : Botão 1

Pause : Botão 2

Copiar o arquivo "SMS.MC2" para a raiz do cartão SD. Os jogos podem estar em pastas.

Os jogos de Game Gear deverão conter a extensão ".GG"

##### Change log

- 002 : 19/10/2019 - novo core com suporte a som FM e jogos do Game Gear, baseado no MiST
- 001 : 04/12/2016 - versão inicial