# Arcades

Copie o arquivo ".MC2" da máquina desejada para a raiz de um cartão SD, formatado em FAT ou FAT32.

##### Observações

Alguns hardwares são necessários dois arquivos. Descompacte o ZIP na raiz do cartão SD.

##### Bugs conhecidos

Algumas máquinas tem orientação vertical.

O hardware que gera as estrelas de fundo do Galaxian não esta corretamente sintetizado.

Super Glob parece que usa um botão de ação, que não está sintetizado no hardware.

Alguns arcades usam temporizações um pouco fora do padrão VGA, que pode não ser compatível com todos os monitores. 

Algumas sintetizações tem alguns pixels das bordas cortadas, por não caberem na resolução do VGA.

##### Change log

- 022 - 25/03/2020 - Adicionado Lunar Rescue, Ozma Wars, Vortex, Space Laser, Super Earth Invasion e Space Invaders (hardware Midway/Taito 8080). )

- 021 - 27/01/2020 - Adicionado Colony 7, Defender, Mayday e Jin (Willians 6809 rev.1 hardware)

- 020 - 25/01/2020 - Adicionado Ninja-Kun (Nova 2001 hardware) e Rampage, Sarge, Power Drive e Max RPM (Midway MCR 3 Monoboard)

- 019 - 02/01/2020 - Adicionado Berzerk, Frenzy e Moon War (Berzerk hardware). Donkey Kong e Donkey Kong Jr. (Nintendo Radar Scope hardware)

- 018 - 01/01/2020 - Adicionado Burger Time e Burnin Rubber. (Data East Burger Time hardware). Cosmic Avenger, Dorodon, Ladybug e SnapJack. (Ladybug hardware). Bagman, Botanic, Pickin, Squase e Super Bagman (Bagman hardware). Canyon Bomber, Dominos, Sprint One, Sprint 2, Super Breakout e Ultratank (Atari BW Raster hardware)

- 017 - 28/12/2019 - Adicionado Azzurian Attack, Blackhole, Catacomb, Chewing Gum, Devil Fish, Galaxian, King & Ballons, Moon Cresta, Mr. Do's Nightmare, Omega, Orbitron Pisces, Triple Draw, Uniwars, Victory, War of the Bugs, Zig Zag. (Pacman hardware).

- 016 - 27/12/2019 - Adicionado Phoenix, Pleiads e Capitol (Phoenix hardware).  Armored Car, Tazz Mania e Moon War (Scramble hardware).

- 015 - 20/12/2019 - Adicionado Tetris, Green Beret e Bomb Jack.

- 014 - 14/12/2019 - Adicionado Amidar, The End e Speed Coin.

- 013 - 24/10/2019 - Adicionado Centipede e Dig Dug.

- 012 - 23/10/2019 - Adicionado Asteroids.

- 011 - 16/10/2019 - Adicionado Super Cobra (preliminar), Scramble e Frogger.

- 010 - 15/10/2019 - Adicionado Crazy Climber e Crazy Kong.

- 009 - 12/10/2019 - Adicionado Traverse USA.

- 008 - 11/10/2019 - Adicionado New Rally X.

- 007 - 09/10/2019 - Adicionado Vulgus e Gun Smoke. Revisão no 1942, agora perfeito. Suporte a joystick Sega 3/6 botões em todos os jogos da placa Campcom.

- 006 - 07/10/2019 - Adicionado versões iniciais dos jogos da placa Capcom (Ghost and Goblins, Commando, 1942 e 1943)

- 005 - 23/07/2019 - Xevious e Time Pilot finais, com novos loaders e saida HDMI.

- 004 - 01/11/2018 - update hardware do Time Pilot.

- 003 - 12/04/2018 - hardware do Time Pilot.

- 002 - 24/12/2016 - hardware do Pac-man atualizado e ampliado. Incluido Roller Crusher, Gorkans, Mr TNT, Ms Pac-Man e Super Glob. Versão inicial do hardware da Galaxian.

- 001 - 24/11/2016 - versão inicial Pac-Man, Scramble e Frogger.