# Amstrad CPC

Microcomputador Amstrad CPC 128k para o Multicore 2.

Descompactar e copiar os arquivos "Amstrad.MC2", "Amstrad.dat" para a raiz do cartão SD.

Botão 1: Soft Reset

Botão 1 + botão 2: Hard Reset 

F12: abre o menu para seleção de arquivos de imagens.

Imagens de tape devem ter a extensão "CDT". Imagens de disk devem ter a extensão "DSK". As imagens podem estar em pastas dentro do cartão SD.

As imagens de tape podem ser carregadas normalmente pelo player interno, basta dar o comando de carregamento correspondente do CPC. 
O player começa a tocar o audio automaticamente entre 15 a 30 segundos após o comando de carregamento. Basta apenas aguardar.

##### Bugs conhecidos

- Alguns jogos são PAL e alteram a frequencia de video para cerca de 50Hz, que não é compatível com todos os monitores.


##### Change log

- 002 : 25/10/2019 - Corrigida a interface de disco.

- 001 : 24/10/2019 - Versão inicial
