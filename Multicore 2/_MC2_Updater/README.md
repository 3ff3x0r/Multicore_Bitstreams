# Multicore 2 atualizador

O Multicore 2 funciona com base em dois "firmwares" diferentes. 

O primeiro funciona no microcontrolador STM32 e controla o carregamento dos cores, enquanto o segundo é diretamente na memória flash do FPGA, montando o menu inicial e fazendo a leitura do teclado e joystick.

Não acredito que será um procedimento comum atualizar quaisquer desses firmwares uma vez que o projeto já estiver em pleno funcionamento, mas como nunca sabemos o dia de amanha, um método de atualização se mostrou necessário devido ao fato do MC2 não utilizar cabos conectados ao PC. No nosso caso utilizaremos o próprio cartão SD para fazer a transferência e atualização dos cores.

A versão atual dos firmwares são informados na tela inicial do MC2, logo depois de ligá-lo.
[![ScreenShot](Menu1.JPG)]
[![ScreenShot](Menu2.JPG)]

O procedimento de atualização é muito simples, porém exige pequenos cuidados.

#### Atualizando o firmware do STM
Arquivo
https://gitlab.com/victor.trucco/Multicore_Bitstreams/-/blob/master/Multicore%202/_MC2_Updater/STM_update_V108.zip

Descompacte o conteúdo do zip em um cartão recém-formatado (não importa o formato, FAT e FAT32 funcionam). O cartão não deve conter outros arquivos além dos que estão no zip. Não misture arquivos do CORE e do STM. Faça as atualizações separadamente, uma por vez, formatando novamente o cartão entre as atualizações.

Quando o atualizador for carregado, é possivel notar a borda na cor azul claro e um pedido para trocar o jumper do microcontrolador.
[![ScreenShot](Step_1_1.JPG)]

Infelizmente para ter acesso a este jumper é necessário remover a tampa de acrilico do MC2.
[![ScreenShot](stm32.jpg)]

A posição correta para a atualização é mover o jumper da esquerda para a posição 1.
[![ScreenShot](stm32_02.jpg)]

Após pressionar ENTER e confirmar, a atualização se inicia.
[![ScreenShot](Step_1_2.JPG)]

**ATENÇÃO: não desligue o MC2 durante a atualização**. 

Ao final do processo, vemos a confirmação (borda verde) e o pedido para voltar com o jumper para a posição inicial.
[![ScreenShot](Step_1_3.JPG)]

**ATENÇÃO: se houver alguma mensagem de erro, NÃO DESLIGUE**. Basta pressionar o botão 1 do MC2 que todo o processo se reinicia. 
[![ScreenShot](Step_1_error.JPG)]

#### Atualizando o firmware do CORE
Arquivo
https://gitlab.com/victor.trucco/Multicore_Bitstreams/-/blob/master/Multicore%202/_MC2_Updater/CORE_update_V105.zip

Descompacte o conteúdo do zip em um cartão recém-formatado (não importa o formato, FAT e FAT32 funcionam). O cartão não deve conter outros arquivos além dos que estão no zip. Não misture arquivos do CORE e do STM. Faça as atualizações separadamente, uma por vez, formatando novamente o cartão entre as atualizações.

Quando o atualizador for carregado, é possivel notar a borda na cor azul escura e um pedido para confirmar a atualização
[![ScreenShot](Step_2_1.JPG)]

Logo após a atualização se inicia.
[![ScreenShot](Step_2_2.JPG)]

**ATENÇÃO: não desligue o MC2 durante a atualização**. 

Ao final do processo, vemos a confirmação (borda verde) e o pedido para desligar a fonte do MC2.
[![ScreenShot](Step_2_3.JPG)]



##### Change log

- 008 - 17/03/2020
		
		**STM versão 1.08**

		- arquivos .INI para os cores
		
- 007 - 09/03/2020
		
		**STM versão 1.07**
		
		- Splash Screen 
		- Ordenação alfabética de pastas e arquivos
		- Possibilidade de Cores dentro de pastas
		- 128 arquivos por pasta
		- Novo gerenciamento de teclado
		- Navegação pelas letras no menu
		- Auto-repetição de teclas
		
		
		**CORE versão 1.05**
		
		- Gerenaciamento do teclado
		- Auto-teste (botão 4)
 		
		
- 006 - 25/01/2020 - STM versão 1.06. CORE versão 1.04
- 005 - 02/12/2019 - STM versão 1.05. CORE versão 1.03
- 004 - 16/07/2019 - STM versão 1.04.
- 003 - 31/03/2019 - STM versão 1.03.
- 002 - 15/05/2018 - STM versão 1.02. CORE versão 1.01 - Obrigado ao Mauro Passarinho pela ajuda nos testes.
- 001 - 12/04/2018 - STM versão 1.01. CORE versão 1.01
