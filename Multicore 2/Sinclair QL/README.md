# Sinclair QL

Microcomputador Sinclair QL para o Multicore 2.

Utilize a tecla F12 para usar o menu e carregar o microdrive. Os arquivos tem a extensão .MDV e tamanho de 174930 bytes.

Copiar os arquivo "QL.MC2" e "QL.DAT" para a raiz ou pasta no cartão SD.


##### Change log

- 002 : 11/04/2020 - Suporte a menu de opções e ROMs alternativas
- 001 : 10/05/2018 - versão inicial