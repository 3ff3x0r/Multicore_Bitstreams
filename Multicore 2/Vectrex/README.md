# Vectrex

Videogame Vectrex para o Multicore 2.

Os jogos utilizam de 1 a 4 botões, então é recomendado o uso do joystick da Sega, padrão 6 botões.

Utilize a tecla F12 ou botao "start" do joystick para usar o menu e carregar os jogos.

O botão 1 faz o soft-reset do core.


Copiar o arquivo "Vectrex.MC2" para a raiz do cartão SD.

##### Change log

- 001 : 12/04/2018 - versão inicial