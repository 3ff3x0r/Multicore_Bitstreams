# ZX81

- VGA
- suporte a interface de som ZON-X
- Redefinição de caracteres CHR128
- Carregamento via conector auxiliar
- Carregamento de arquivos .P direto pelo menu
- Vsync ligado ao audio para simular "interferência" gerada por alguns programas

Hard Reset: Botão 1

#### Uso

Coloque o arqui .MC2 no raiz do seu cartão SD. Os jogos .P podem estar em pastas. Use F12 para abrir o menu de opções. Para carregar um programa, selecione a opção "LOAD *.P" no menu, escolha o arquivo e em seguida digite LOAD "" e pressione ENTER.

ATENÇÃO! Necessita firware STM v.1.05 e CORE v.1.03

##### Change log

- 004 : 02/12/2019 - Menus de configuração adicionados
- 003 : 13/10/2019 - nova versão, port do ZX8X
- 002 : 09/07/2018 - versão inicial Multicore 2
- 001 : 25/01/2016 - versão inicial