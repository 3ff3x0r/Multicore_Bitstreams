# Imagens de disco

A controladora do Apple II/TK2000 espera uma sequencia de discos "nibblized" (227k), mas imagens de disco est�o em formato DSK de 140Kb, ent�o precisam ser convertidas antes para o formato NIB. O pequeno programa "AppleDSK2NIB.exe" cumpre este trabalho, convertendo a imagem.

Basta selecionar o DSK, bot�o "Select File" e logo ap�s selecionar "Convert". O novo arquivo tem o mesmo nome do arquivo de origem, por�m com a extens�o .NIB. Estes s�o os arquivos que ser�o usados no Multicore 2.
