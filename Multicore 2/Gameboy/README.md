# Gameboy

Console Gameboy para o Multicore 2.

Utilize a tecla F12 para usar o menu e carregar os jogos. Os arquivos tem a extensão GB.

Copiar os arquivo "Gameboy.MC2" para a raiz do cartão SD.

Botão 1 - Reset

Botão 2 - Cor do LCD (preto & branco ou amarelo)

É necessário um joystick padrão Sega 3 ou 6 botões.

##### Change log

- 001 : 18/07/2018 - versão inicial