# TBBlue

Formato do SD: FAT16 ou FAT32, porém o sistema operacional divMMC ainda não suporta nomes longos de arquivo.

Descompactar o arquivo "SD Card.rar" na raiz do cartão SD, após a formatação.

A máquinas sintetizadas são ZX Spectrum e compatíveis.

- ZX Spectrum 48Kb
- ZX Spectrum 128kb
- ZX Spectrum +3
- TK90X/TK95

Emuladas
- ZX80
- ZX81/TK85

# Suporte ao CI de som SID
Use .SIDPLAY para tocar as musicas na pasta /SIDDUMPS
Exemplo:

.cd siddumps

.ls

.sidplay rtype.dmp

##### Change log

- FW 1.10a - CORE 1.10.52 : 30/08/2018 - versão inicial para Multicore 2