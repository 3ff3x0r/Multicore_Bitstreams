# Nintendo Entertainment System

Formato do SD: FAT16 ou FAT32 com nomes longos de arquivos, compatível com arquivos ".NES"

Copiar o arquivo "NES.MC2" para a raiz do cartão SD.

Botão 1: Start player 1

Botão 2: Select player 1

Botão 3: Start player 2

Botão 4: Select player 2


Reset: Botão 3 + Botão 4 volta à tela de menu do Loader


Scanlines: pode ser selecionadas diretamente no menu do Loader.


A memória foi dividida em 256Kb para a PRG ROM e 256Kb para a CHR ROM. 
Não conheço nenhum que ultrapasse esses limites, mas teoricamente podem existir jogos que usem mais.



##### Bugs conhecidos


- Algumas vezes ocorre erro durante o carregamento da ROM. Se algum jogo não carregar na primeira vez, basta repetir a operação e o jogo é carregado.







##### Change log

- 004 : 24/06/2019 - Detecção joystick padrão de Mega Drive
- 003 : 26/08/2018 - Correção botão START
- 002 : 20/02/2018 - Suporte ao Multicore 2
- 001 : 17/12/2016 - versão inicial
