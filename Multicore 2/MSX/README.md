# MSX

Formato do SD: FAT16

Descompactar o arquivo "SD Card.rar" no cartão SD, após a formatação.

Copiar o arquivo "MSX.MC2" na raiz do cartão SD.

A máquina sintetizada é um MSX 1.0 de 128Kb de memória e som SCC.


##### Change log

- 002 : 19/01/2018 - versão para Multicore 2
- 001 : 24/11/2016 - versão inicial