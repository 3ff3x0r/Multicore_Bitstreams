# Imagens de disco

A controladora do Apple II espera uma sequencia de discos "nibblized" (227k) no SD a partir do bloco 0. 
Muitas imagens de disco de Apple est�o em formato DSK de 140Kb, ent�o precisam ser convertidas antes para o formato NIB. O pequeno programa "AppleDSK2NIB.exe" cumpre este trabalho, convertendo a imagem.

Ap�s a convers�o as imagens podem ser concatenadas em um �nico arquivo para facilitar a escrita no cart�o SD.

No prompt de comando do Windows podemos usar o pr�prio COPY do sistema para isso:

COPY /b disco1.nib + disco2.nib + disco3.nib imagem_final.img /b

Com a imagem final, basta escrever no cart�o com um programa como o Win32DiskImager. Note que n�o � colocar o arquivo IMG gerado no passo acima dentro de um cart�o formatado em FAT, mas sim escrever a imagem no cart�o, o que eu chamo de formato RAW.