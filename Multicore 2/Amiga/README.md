# Amiga

Microcomputador Amiga para o Multicore 2.

Copiar os arquivos "Amiga.MC2", "Kick.ROM" e "OSD68K01" para a raiz do cartão SD.

Utilize a tecla F12 para usar o menu e carregar os jogos. Os arquivos tem a extensão ADF.

O joystick deve ser utilizado na porta de Joystick 2 (DB9 direito no Multicore)

Requer Teclado (porta PS/2 erquerda) e Mouse (porta PS/2 direita)

##### Change log

- 002 : 09/08/2018 - Acerto no Joystick e suporte a Sega 3 ou 6 botões
- 001 : 11/07/2018 - Versão inicial