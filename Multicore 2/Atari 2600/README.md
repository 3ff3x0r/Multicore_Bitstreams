# Atari 2600

Formato do SD: FAT16 ou FAT32, com suporte a nomes logos de arquivo.

Copiar o arquivo "ATARI.MC2" na raiz do cartão SD.

F12 pressionado por 1 segundo abre o loader.

Reset: Botão 3 + Botão 4 volta à tela do SD Loader

Scanlines opcionais: Botão 1 + Botão 2 ativam/desativam scanlines

Seleção Difficuldade Jogador 1 : Botão 3

Seleção Difficuldade Jogador 2 : Botão 4

Seleção chave Coulour/PB : Botão 3 + botão do joystick

O loader sempre tenta identificar o esquema de bankswitching do cartucho pelo próprio arquivo carregado, porém é possível forçar algum dos esquemas, apenas alterando a extensão do arquivo.

Exemplo: renomear "HERO.BIN" para "HERO.F8" força o carregamento do arquivo em modo F8

Extensões

.2K Atari 2K

.4K Atari 4K (default)

.F8 Atari F8

.F8S Atari F8 com Superchip

.F6 Atari F6

.F6S Atari F6 com Superchip

.F4 Atari F4

.F4S Atari F4 com Superchip

.FA CBS RAM +

.FE Activision FE

.3F Tigervision 3F

.3E mode 3E (3F com 4K RAM)

.E0 Parker Brothers E0

.E7 M-Network E7

.DPC Pitfall II

.CV Commavid

.UA UA-Ltd

##### Bugs conhecidos
 

Alguns jogos perdem o sincronismo da imagem durante o jogo

- Boxe
- Wizard of Wor
- Frogger


##### Change log

- 007 : 21/01/2020 - Bankswitchs CV, E7, UA e FA. Modificações nos CIs para melhor compatibilidade.

- 006 : 28/09/2018 - Novo módulo de audio. (Sons corretos no Pitfall por exemplo)

- 005 : 07/09/2018 - Novo loader baseado no microcontrolador STM32 e saida de video em HDMI.

- 004 : 04/06/2018 - Adicionado suporte a Pitfall II, Decathlon e Robot Tank.

- 003 : 19/11/2016 - OSD para seleção de difficuldade e chave Colour/PB. Acerto nas cores dos scanlines.

- 002 : 26/11/2016 - Scanlines opcionais incluidas

- 001 : 24/11/2016 - versão inicial