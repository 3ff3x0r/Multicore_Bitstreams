# Multicore Bitstreams

Bistreams pronto para uso no Multicore e no Multicore 2. (Atenção, pastas separadas para as duas versões)

https://www.victortrucco.com/Multicore/Multicore

Consulte informações específicas de cada Core dentro das pastas específicas.

Os bitstreams estão disponíveis nas versões ".SOF" para programação do Multicore em soft-mode onde a programação do FPGA é perdida quando a fonte de alimentação é desligada e ".JIC" para a programação da memória flash da placa. Uma vez programada a flash, a placa pode ser iniciada sem o cabo conectado, com o último bitstream .JIC programado. O uso de um .SOF não apaga o .JIC da placa, sendo ideal para experimentar outras plataformas sem precisa reprogramar a flash todas as vezes. 
J
Alguns Cores tem saída de vídeo em HDMI ou VGA. Para o caso do Multicore, note que é preciso programar a versão de sua preferência na placa, já que não é possível utilizar as duas saídas de vídeo ao mesmo tempo por limitações do hardware. 

Para gravação dos Cores no Multicore é necessário ter instalado a ferramenta de programação stand-alone da Altera.

https://www.victortrucco.com/Multicore/MulticoreGravacao


A versão completa do Quartus II também poderá ser usada, mas o arquivo para instalação é substancialmente maior.
https://www.intel.com/content/www/us/en/software-kit/666221/intel-quartus-ii-web-edition-design-software-version-13-1-for-windows.html


Sugestões de etiquetas para cartões SD. (Autor: Mauro Passarinho)
https://gitlab.com/victor.trucco/Multicore_Bitstreams/blob/master/Labels.rar

Sobre o TBBlue:
https://www.victortrucco.com/TK/TBBlue/TBBlue