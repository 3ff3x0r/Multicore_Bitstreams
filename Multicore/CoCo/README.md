# Tandy Color Computer

Formato do SD: RAW, formato próprio. Deve ser adotado o mesmo procedimento de montar o cartão para a MiniIDE, descrita neste artigo do Daniel Campos.
http://amxproject.com/?p=2608

Soft Reset : Control + ESC

Hard Reset: Botão 3 + Botão 4

Scanlines opcionais: Botão 1 + Botão 2 ativam/desativam scanlines

Inverter portas dos joysticks : Botão 1

Artefatos NTSC: Botão 2 cicla todas as possibilidades


##### Change log

- 003 : 23/04/2018 - Correção na centralização dos joysticks
- 002 : 20/02/2018 - Suporte ao Multicore 2
- 001 : 10/12/2016 - versão inicial
