# CCE MC1000

Formato do SD: Sem suporte a SD no momento. Os arquivos podem ser lidos pela entrada auxiliar do Multicore.

Entrada Auxiliar: Plug P2 estéreo, ponta do plug é a saída MIC para gravação de dados, conexão do meio é o EAR para a leitura e a carcaça é o terra.

Hard Reset: Botão 3 + Botão 4

Scanlines opcionais: Botão 1 + Botão 2 ativam/desativam scanlines


##### Change log

- 001 : 06/12/2016 - versão inicial