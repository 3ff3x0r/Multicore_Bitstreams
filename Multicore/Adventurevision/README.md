# Adventurevision

- Não usa SD.

Botão 1 : Soft Reset

Botão 3 + botão 4: Hard Reset, mudando o jogo



##### Observações

- Este videogame só tem quatro cartuchos lançados e estão gravados diretamente na memória flash. 
Por este motivo, apenas o arquivo .JIC foi disponibilizado.

- Após a troca do jogo, é necessário um soft reset. 
Segure o botão por um ou dois segundos para que a memória seja apagada do jogo anterior.

- Devido ao número de botões, os controles são somente pelo teclado PS/2:

Setas - direcionais

Z, X, C, V - Botão 1, 2, 3 e 4

##### Change log

- 001 : 17/12/2016 - versão inicial