# CP/M

Arquivos JIC e SOF, de acordo com a versão, VGA ou HDMI.

Necessita de um cartão SD em formato próprio. Descompacte o zip e utilize um programa como o Etcher ou Win32DiskImage para gravar a imagem no cartão.

Este core é baseado no projeto de autoria de Grant Searle. http://searle.hostei.com/grant/Multicomp/cpm/fpgaCPM.html

##### Change log

- 003 : 14/08/2018 - Incluida a imagem para o cartão SD
- 002 : 20/02/2018 - Suporte ao Multicore 2
- 001 : 24/11/2016 - versão inicial