# Commodore 64 IMG generator

Executavel para Windows, mas código fonte incluído para ser recompilado em outras plataformas.

No core do C64, o SD é lido como um repositórios de imagens D64 (imagens de 174 848 bytes). Cada imagem deve ficar em limites de 256K.
Então, no arquivo de imagem, temos em 0x00000 o primeiro D64, 0x40000 para o segundo, 0x80000 o terceiro, 0xC0000 o quarto, 0x100000 o quinto, 0x140000 o sexto e assim seguindo o mesmo padrão para todas as próximas imagens.

Basta colocar todas as imagens .D64 dentro de uma pasta junto com o programa e executá-lo. O arquivo final .IMG com todas as imagens concatenadas pode ser transferido para o cartão pelo programa Win32DiskImage ou pelo "dd" no Linux/MacOS.
Para facilitar a organização, é gerados um arquivo texto com a numeração das imagens para utilização no core.

##### Change log

- 001 : 08/09/2018 - versão inicial